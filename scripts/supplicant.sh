#/bin/sh
echo Launching wpa_supplicant in wpa3_cont
docker run -it --privileged=true --network=host wpa3_cont /workspace/hostap/wpa_supplicant/wpa_supplicant -i wlan1 -c /workspace/hostap/supp_wpa3.conf -dd -K
