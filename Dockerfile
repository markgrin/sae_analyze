FROM debian

RUN apt-get update && \
    apt-get install -y vim git gcc make pkg-config \
    libssl-dev libnl-3-dev libnl-genl-3-dev

RUN mkdir workspace && \
    git clone git://w1.fi/hostap.git && \
    mv hostap workspace && \
    cd /workspace/hostap/ && \
    git checkout 656b07c189

RUN cd workspace/hostap/wpa_supplicant && \
    cp defconfig .config && \
    sed -i 's/CONFIG_CTRL_IFACE_DBUS/#DISABLE_DBUS/g' .config && \
    echo "NEED_AP_MLME=y" >> .config && \
    cd ../hostapd && \
    cp defconfig .config && \
    sed -i 's/CONFIG_CTRL_IFACE_DBUS/#DISABLE_DBUS/g' .config && \
    echo "NEED_AP_MLME=y" >> .config && \
    echo "CONFIG_SAE=y" >> .config
    
RUN cd workspace/hostap/wpa_supplicant && \
    make clean && \
    make

RUN cd workspace/hostap/hostapd && \
    make clean && \
    make

COPY confs/hostapd_wpa3.conf /workspace/hostap
COPY confs/supp_wpa3.conf /workspace/hostap


